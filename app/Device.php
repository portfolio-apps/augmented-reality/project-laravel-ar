<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class Device extends Model
{
    const DEFAULT_ID = "eb7cb373-af76-49f0-87bb-c614a21c46f9";

    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $appends = [
        'temp'
    ];

    // protected static function boot()
    // {
    //     parent::boot();
    //     static::creating(function ($model) {
    //         $model->id = (string) Str::uuid();
    //     });
    // }

    // public function organization()
    // {
    //     // this ORganization has many Tickets
    //     return $this->belongsTo(Organizaiton::class);
    // }

    public function markers()
    {
        return $this->hasMany(Marker::class);
    }

    public function getTempAttribute()
    {
        $data = Redis::get("cpu_temp:{$this->id}");
        return $data;
    }
}
