<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\MqttHelper;
use Illuminate\Support\Facades\Redis;
use App\Device;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::with(['markers'])->get();

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'devices' => $devices
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate Request Fields
        $valid = validator($request->all(), [
            'name' => 'min:5',
            'ip' => 'ipv4'
        ]);
        // Check if valid if not throw error for field
        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $device = new Device();
        // $device->organization_id = auth()->user()->organization_id;
        $device->name = $request->name;
        $device->ip_address = $request->ip_address;
        $device->save();

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'device' => $device
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = Device::findOrFail($id);

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'device' => $device
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate Request Fields
        $valid = validator($request->all(), [
            'name' => 'min:5',
            'ip' => 'ipv4'
        ]);
        // Check if valid if not throw error for field
        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $device = Device::where([
            'organzation_id' => auth()->user()->organization_id,
            'id' => $id,
        ])->first();
        if (!$device) {
            return response()->json([
                'success' => false,
                'message' => "No resource found"
            ], 404);
        }
        $device->name = $request->name;
        $device->ip_address = ($request->ip_address) ? $request->ip_address : $device->ip_address;
        $device->save();

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'device' => $device
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $device = Device::findOrFail($id);
        $device->delete();
        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'message' => "Deleted Device: {$id}"
            ]);
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function connect($id)
    {
        $device = Device::where([
            'id' => $id,
        ])->first();
        if (!$device) {
            return response()->json([
                'success' => false,
                'message' => "No resource found"
            ], 404);
        }

        $topic = "connect/{$id}";
        $msg['command'] = 'New ngrok connection';
        $pub = MqttHelper::publish($topic, $msg);

        if (!$pub) {
            return response()->json([
                'success' => false,
                'message' => "No message was published"
            ], 500);
        }

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'message' => "Connected to: {$id}"
            ]);
        } else {
            // Render view with data
            return response()->json([
                'success' => false,
                'message' => "No message was published"
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function command(Request $request, $id)
    {
        // Validate Request Fields
        $valid = validator($request->all(), [
            'command' => 'required|string|min:2'
        ]);
        // Check if valid if not throw error for field
        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $device = Device::where([
            'id' => $id,
        ])->first();
        if (!$device) {
            return response()->json([
                'success' => false,
                'message' => "No resource found"
            ], 404);
        }

        $topic = "command/{$id}";
        $msg['command'] = $request->command;
        $pub = MqttHelper::publish($topic, $msg);
        if (!$pub) {
            return response()->json([
                'success' => false,
                'message' => "No message was published"
            ], 500);
        }

        $topic_get = str_replace('/', ':', $topic);
        $prev = Redis::get("response:{$topic_get}");
        $data = str_replace("\n", "", $prev);

        if (str_contains($request->command, '.tunnels[0].public_url')) {
            Redis::set("current_feed:{$id}", $data);
            $current_vid = Redis::get("current_feed:{$id}");
        }

        if (request()->is('api/*') == 1 || request()->is('vue/*') == 1) {
            return response()->json([
                'success' => true,
                'message' => "Running '{$request->command}' on {$id}",
                'data' => $data
            ]);
        } else {
            // Render view with data
            return response()->json([
                'success' => false,
                'message' => "No message was published"
            ], 500);
        }
    }
}
