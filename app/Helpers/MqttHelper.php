<?php
namespace App\Helpers;

use Illuminate\Support\Facades\App;

use Mqtt;

class MqttHelper
{
  /**
   * MQTT Publish
   *
   * @return \MQTT\Publish
   */
  public static function publish($topic, $msg)
  {
    $output = Mqtt::ConnectAndPublish($topic, json_encode($msg));

    if ($output === true)
    {
      return true;
    }

    return false;
  }

  /**
   * MQTT Subscribe
   *
   * @return \MQTT\Subscribe
   */
  public static function subscribe($topic)
  {
    return Mqtt::ConnectAndSubscribe($topic, function($topic, $msg){
      echo "Msg Received: \n";
      echo "Topic: {$topic}\n\n";
      echo "\t$msg\n\n";
    },"HELOO");
  }
}
