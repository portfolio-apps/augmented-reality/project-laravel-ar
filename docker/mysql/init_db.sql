-- Create Databases
CREATE DATABASE IF NOT EXISTS `arstack`;
CREATE DATABASE IF NOT EXISTS `arstack_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'arstack'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;