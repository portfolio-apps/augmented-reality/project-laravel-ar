require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');

// const redisObj = {
//   url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
// }
const redisObj = {
  url: process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
}

const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect(process.env.MQTT_PROTOCOL + process.env.MQTT_URL || "3.142.247.111", {
  host: process.env.MQTT_URL || "3.142.247.111",
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: (process.env.MQTT_CERT_VERIFY === 'true')
});

/**
 * Topic subscribed too
 */

const topic = 'cpu_temp/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
  let topic_str = topic.toString()
  // split the topic str into array
  topic_str = topic_str.split('/')
  console.log(topic)
  
  const temp = message.toString().replace('temp=', '').replace(/^\s+|\s+$/g, '');
  console.log(temp)

  redisClient.setex(`${topic_str[0]}:${topic_str[1]}`, 15000, temp, redis.print);
})

console.log(`cpu.js started successfully ${(new Date).toISOString()}`)
