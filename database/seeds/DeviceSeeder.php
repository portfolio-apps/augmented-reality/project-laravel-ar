<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

use App\Device;
use App\Marker;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $device = new Device;
        $device->id = Device::DEFAULT_ID;
        $device->name = "Waltcam";
        $device->ip_address = "104.15.248.11";
        $device->save();

        $marker = new Marker;
        $marker->id = Marker::DEFAULT_ID;
        $marker->description = "This is the Marker For the Waltcam Raspi";
        $marker->device_id = $device->id;
        $marker->marker_url = 'https://siasky.net/CAAMkTrDFDEatrfM0rYkgP1Wov-BF4RQ_INqhREeZmaJog';
        $marker->pattern_url = 'https://siasky.net/GACKwrTK8723KfzUCyPw9-E26yrZNdyt5PBkxoNvucQliA';
        $marker->save();

        $marker = new Marker;
        $marker->id = Str::uuid();
        $marker->description = "Raspi Uptime";
        $marker->device_id = $device->id;
        $marker->marker_url = 'https://siasky.net/EADALecoLA4FyPZLaVDQsZNYj9IhRktUd_hdBuy4YBsKTA';
        $marker->pattern_url = 'https://siasky.net/GADNDBP2rcDOumINJixoFyfOnw-2Sy97lDv4wiKshVhkXQ';
        $marker->save();
    }
}