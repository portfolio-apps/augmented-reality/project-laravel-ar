<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel AR - Viewer</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
        <!-- we import arjs version without NFT but with marker + location based support -->
        <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar.js"></script>

    </head>
    <body style="margin : 0px; overflow: hidden;">
           
        <div style='position: fixed; top: 10px; width:100%; text-align: center; z-index: 100;'>
            <button onclick="goBack()">
                Go Back
            </button>
        </div>
        <a-scene embedded arjs>
            {{-- <a-marker type="pattern" url="https://siasky.net/GACKwrTK8723KfzUCyPw9-E26yrZNdyt5PBkxoNvucQliA">
                <a-box rotation="-90 0 0" position='0 0.5 0' material='opacity: 0.9;' color="blue">
                    <a-entity id="hello" position="2.1 0 0.7" scale="5 5 5"></a-entity>
                </a-box>
            </a-marker>
                
            <a-marker type="pattern" url="https://siasky.net/GADNDBP2rcDOumINJixoFyfOnw-2Sy97lDv4wiKshVhkXQ">
                <a-box id="box2" rotation="-90 0 0" position='0 0.5 0' material='opacity: 0.9;' color="pink">
                    <a-entity id="hello2" position="2.1 0 0.7" scale="5 5 5"></a-entity>
                </a-box>   
            </a-marker> --}}

            @foreach ($markers as $marker)
                <a-marker type="pattern" url="{{ $marker->pattern_url }}">
                    <a-box id="box_{{ $marker->id }}" rotation="-90 0 0" position='0 0.5 0' material='opacity: 0.9;' color="green">
                        <a-entity id="entity_{{ $marker->id }}" position="2.1 0 0.7" scale="5 5 5"></a-entity>
                    </a-box>   
                </a-marker>
                <script>
                    setInterval( async function(){ 
                        try {
                            const stuff = await axios.get('/redis');
                            document.getElementById(`entity_{{ $marker->id }}`).setAttribute('text', `value: ${stuff.data.temp};color:white`);
                        } catch(e) {
                            console.error(e)
                        }
                    }, 2000);
                </script>
            @endforeach

            <a-entity camera></a-entity>
        </a-scene>

        <script>
            // setInterval( async function(){ 
            //    try {
            //     const stuff = await axios.get('/redis');
            //     document.getElementById('hello').setAttribute('text', `value: ${stuff.data.temp};color:white`);
            //    } catch(e) {
            //        console.error(e)
            //    }
            // }, 2000);
            // setInterval(function(){ 
            //     const someVal = Math.round(Math.random()*100);
            //     let color = ''

            //     if (someVal < 50) {
            //         color = 'green'
            //     } else if (someVal > 50 && someVal < 75) {
            //         color = 'orange'
            //     } else {
            //         color = 'red'
            //     }

            //     document.getElementById('box2').setAttribute('color', color);
            //     document.getElementById('hello2').setAttribute('text', `value: ${someVal} PSI;color:white`);
            //     console.log(someVal);
            // }, 2000);
            function goBack() {
                window.history.back();
            }
        </script>
    </body>
</html>
