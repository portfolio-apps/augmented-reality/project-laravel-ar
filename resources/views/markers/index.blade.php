@extends('layouts.main')
@section('content')

    <div class="flex-center position-ref full-height" style="background-color: rgb(235, 235, 235); height: 100vh">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif

        <div class="content">
            
          <button onclick="goBack()" href="#">
            Go Back
          </button>
          <script>
            function goBack() {
                window.history.back();
            }
          </script>
           <div class="container mt-5">
              <div class="row mb-3">
                <div class="col-sm-12">
                  <h2>ARstack</h2>
                </div>
              </div>
             <div class="row">
               

                @if (count($markers) > 0)
                  @foreach ($markers as $marker)
                    <div class="col-sm-3 mb-2">
                      <div class="card">
                        {{-- <img src="{{ Identicon::getImageDataUri($marker->id, 200) }}" class="img-thumbnail mx-auto" alt="Identicon"> --}}
                        <img src="{{ $marker->marker_url }}" class="img-thumbnail mx-auto" alt="Identicon">
                        <div class="card-body">
                          <p class="card-text">{{ $marker->description }}</p>
                        </div>
                      </div>
                    </div>
                  @endforeach
                @else
                  No Markers Found
                @endif
             </div>
           </div>
        </div>
    </div>
@endsection
